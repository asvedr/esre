use std::rc::Rc;

use serde_json::Value;

use crate::entities::re::ParseJsonError;
use crate::IRe;

pub trait IJsonParser {
    fn parse_json(&self, value: &Value) -> Result<Rc<dyn IRe>, ParseJsonError>;
}

use std::rc::Rc;

use serde_json::Value;

use crate::entities::re::{ApplyResult, PossibleBegin, ReType, State};

pub trait IRe {
    fn get_type(&self) -> ReType;
    // get type of child for vars and labels
    fn get_value_type(&self) -> ReType {
        self.get_type()
    }
    fn apply(&self, state: &mut State, begin: usize) -> ApplyResult;
    fn show(&self) -> String;
    fn to_json(&self) -> Value;
    fn is_any(&self) -> bool {
        false
    }
    fn is_greedy(&self) -> bool {
        unimplemented!("available only for any_of")
    }
    fn get_children(&self) -> Vec<Rc<dyn IRe>> {
        Vec::new()
    }
    fn compilation_required(&self) -> bool {
        false
    }
    fn possible_begin(&self) -> PossibleBegin;
    fn compile(&self, _next_re: Option<Rc<dyn IRe>>) -> Rc<dyn IRe> {
        unimplemented!(
            "this method must be declared in child together with 'compilation_required'"
        )
    }
}

pub(crate) fn compile_if_required(
    re: Rc<dyn IRe>,
    next_re: Option<Rc<dyn IRe>>,
) -> Rc<dyn IRe> {
    if re.compilation_required() {
        re.compile(next_re)
    } else {
        re.clone()
    }
}

pub(crate) fn is_any(re: Rc<dyn IRe>, predicate: &dyn Fn(&dyn IRe) -> bool) -> bool {
    let mut stack = vec![re];
    while !stack.is_empty() {
        let re = stack.pop().unwrap();
        if predicate(&*re) {
            return true;
        }
        stack.extend(re.get_children().into_iter());
    }
    false
}

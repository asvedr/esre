use std::rc::Rc;

use crate::proto::re::IRe;
use crate::re::match_str;

pub trait IntoRe {
    fn into_ire(self) -> Rc<dyn IRe>;
}

pub trait IntoReVec {
    fn into_ire_vec(self) -> Vec<Rc<dyn IRe>>;
}

impl<T: IRe + 'static> IntoRe for T {
    fn into_ire(self) -> Rc<dyn IRe> {
        Rc::new(self)
    }
}

impl IntoRe for &str {
    fn into_ire(self) -> Rc<dyn IRe> {
        Rc::new(match_str::Re::new(self))
    }
}

impl IntoRe for char {
    fn into_ire(self) -> Rc<dyn IRe> {
        Rc::new(match_str::Re::from_char(self))
    }
}

impl IntoRe for String {
    fn into_ire(self) -> Rc<dyn IRe> {
        Rc::new(match_str::Re::new(&self))
    }
}

impl<T: IntoRe> IntoReVec for T {
    fn into_ire_vec(self) -> Vec<Rc<dyn IRe>> {
        vec![self.into_ire()]
    }
}

impl IntoReVec for Vec<Rc<dyn IRe>> {
    fn into_ire_vec(self) -> Vec<Rc<dyn IRe>> {
        self
    }
}

impl<A: IntoReVec, B: IntoReVec> IntoReVec for (A, B) {
    fn into_ire_vec(self) -> Vec<Rc<dyn IRe>> {
        let (a, b) = self;
        let mut result = a.into_ire_vec();
        result.extend(b.into_ire_vec());
        result
    }
}

impl<A: IntoReVec, B: IntoReVec, C: IntoReVec> IntoReVec for (A, B, C) {
    fn into_ire_vec(self) -> Vec<Rc<dyn IRe>> {
        let (a, b, c) = self;
        let mut result = a.into_ire_vec();
        result.extend(b.into_ire_vec());
        result.extend(c.into_ire_vec());
        result
    }
}

impl<A: IntoReVec, B: IntoReVec, C: IntoReVec, D: IntoReVec> IntoReVec for (A, B, C, D) {
    fn into_ire_vec(self) -> Vec<Rc<dyn IRe>> {
        let (a, b, c, d) = self;
        let mut result = a.into_ire_vec();
        result.extend(b.into_ire_vec());
        result.extend(c.into_ire_vec());
        result.extend(d.into_ire_vec());
        result
    }
}

impl<A: IntoReVec, B: IntoReVec, C: IntoReVec, D: IntoReVec, E: IntoReVec> IntoReVec
    for (A, B, C, D, E)
{
    fn into_ire_vec(self) -> Vec<Rc<dyn IRe>> {
        let (a, b, c, d, e) = self;
        let mut result = a.into_ire_vec();
        result.extend(b.into_ire_vec());
        result.extend(c.into_ire_vec());
        result.extend(d.into_ire_vec());
        result.extend(e.into_ire_vec());
        result
    }
}

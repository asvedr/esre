use std::ops::{Range, RangeInclusive};

pub trait IntoCharRanges {
    fn into_char_ranges(self) -> Vec<(char, char)>;
}

impl IntoCharRanges for char {
    fn into_char_ranges(self) -> Vec<(char, char)> {
        vec![(self, self)]
    }
}

impl IntoCharRanges for &str {
    fn into_char_ranges(self) -> Vec<(char, char)> {
        let mut result = vec![];
        for sym in self.chars() {
            result.push((sym, sym))
        }
        result
    }
}

impl IntoCharRanges for Range<char> {
    fn into_char_ranges(self) -> Vec<(char, char)> {
        let vec: Vec<char> = self.collect();
        if vec.is_empty() {
            return vec![];
        }
        vec![(vec[0], *vec.last().unwrap())]
    }
}

impl IntoCharRanges for RangeInclusive<char> {
    fn into_char_ranges(self) -> Vec<(char, char)> {
        let vec: Vec<char> = self.collect();
        if vec.is_empty() {
            return vec![];
        }
        vec![(vec[0], *vec.last().unwrap())]
    }
}

impl<A: IntoCharRanges, B: IntoCharRanges> IntoCharRanges for (A, B) {
    fn into_char_ranges(self) -> Vec<(char, char)> {
        let (a, b) = self;
        let mut res = a.into_char_ranges();
        res.extend(b.into_char_ranges().into_iter());
        res
    }
}

impl<A: IntoCharRanges, B: IntoCharRanges, C: IntoCharRanges> IntoCharRanges
    for (A, B, C)
{
    fn into_char_ranges(self) -> Vec<(char, char)> {
        let (a, b, c) = self;
        let mut res = a.into_char_ranges();
        res.extend(b.into_char_ranges().into_iter());
        res.extend(c.into_char_ranges().into_iter());
        res
    }
}

impl<A: IntoCharRanges, B: IntoCharRanges, C: IntoCharRanges, D: IntoCharRanges>
    IntoCharRanges for (A, B, C, D)
{
    fn into_char_ranges(self) -> Vec<(char, char)> {
        let (a, b, c, d) = self;
        let mut res = a.into_char_ranges();
        res.extend(b.into_char_ranges().into_iter());
        res.extend(c.into_char_ranges().into_iter());
        res.extend(d.into_char_ranges().into_iter());
        res
    }
}

impl<
        A: IntoCharRanges,
        B: IntoCharRanges,
        C: IntoCharRanges,
        D: IntoCharRanges,
        E: IntoCharRanges,
    > IntoCharRanges for (A, B, C, D, E)
{
    fn into_char_ranges(self) -> Vec<(char, char)> {
        let (a, b, c, d, e) = self;
        let mut res = a.into_char_ranges();
        res.extend(b.into_char_ranges().into_iter());
        res.extend(c.into_char_ranges().into_iter());
        res.extend(d.into_char_ranges().into_iter());
        res.extend(e.into_char_ranges().into_iter());
        res
    }
}

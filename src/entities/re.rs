use std::collections::{BTreeMap, BTreeSet};
use std::error::Error;
use std::fmt;
use std::mem;
use std::str::FromStr;

#[derive(Clone, Copy, Eq, PartialEq, Debug)]
pub enum ReType {
    Any,
    AnyOf,
    CharRange,
    MatchStr,
    Not,
    Opt,
    Repeat,
    Seq,
    Var,
    Begin,
    BeginOf,
    End,
    Label,
}

#[derive(Clone, Copy, Eq, PartialEq, Debug)]
pub struct Range {
    pub begin: usize,
    pub end: usize,
}

impl Range {
    pub fn new(begin: usize, end: usize) -> Range {
        Range { begin, end }
    }
}

pub struct State {
    pub var_layer: Vec<String>,
    pub text: Vec<char>,
}

impl State {
    pub(crate) fn new(source: &str) -> State {
        State {
            var_layer: Vec::new(),
            text: source.chars().collect(),
        }
    }
}

#[derive(Clone)]
pub struct ApplyResponse {
    pub(crate) range: Option<Range>,
    pub(crate) index: usize,
    pub(crate) vars: Option<BTreeMap<String, Vec<Range>>>,
}

impl ApplyResponse {
    pub(crate) fn new_empty(index: usize) -> ApplyResponse {
        ApplyResponse {
            range: None,
            vars: None,
            index,
        }
    }

    pub(crate) fn new_range(index: usize, begin: usize, end: usize) -> ApplyResponse {
        ApplyResponse {
            range: Some(Range { begin, end }),
            vars: None,
            index,
        }
    }

    pub(crate) fn add_var(
        mut self,
        prefix: &[String],
        name: String,
        range: Range,
    ) -> ApplyResponse {
        let mut full_name = prefix.join(".");
        if !full_name.is_empty() {
            full_name.push('.');
        }
        full_name.push_str(&name);
        self.add_var_raw(full_name, vec![range]);
        self
    }

    pub(crate) fn join(mut self, other: ApplyResponse) -> ApplyResponse {
        self.range = match (self.range, other.range) {
            (Some(a), Some(b)) => Some(Range::new(a.begin, b.end)),
            (None, Some(a)) => Some(a),
            (Some(a), None) => Some(a),
            (None, None) => None,
        };
        self.index = other.index;
        if let Some(vars) = other.vars {
            for (name, value) in vars.into_iter() {
                self.add_var_raw(name, value);
            }
        }
        self
    }

    pub(crate) fn join_acc(
        a_opt: Option<ApplyResponse>,
        b: ApplyResponse,
    ) -> ApplyResponse {
        match a_opt {
            Some(a) => a.join(b),
            _ => b,
        }
    }

    fn add_var_raw(&mut self, name: String, ranges: Vec<Range>) {
        let mut vars = mem::replace(&mut self.vars, None).unwrap_or_else(BTreeMap::new);
        if let Some(destination) = vars.get_mut(&name) {
            destination.extend(ranges.into_iter());
        } else {
            vars.insert(name, ranges);
        }
        self.vars = Some(vars);
    }
}

#[derive(Eq, PartialEq, Clone, Copy)]
pub enum ApplyError {
    NotMatched,
    StopSignal,
    Empty,
}

impl ToString for ApplyError {
    fn to_string(&self) -> String {
        use ApplyError::*;

        match self {
            NotMatched => "NM",
            StopSignal => "SS",
            Empty => "E",
        }
        .to_string()
    }
}

impl FromStr for ApplyError {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use ApplyError::*;

        match s {
            "NM" => Ok(NotMatched),
            "SS" => Ok(StopSignal),
            "E" => Ok(Empty),
            _ => Err(()),
        }
    }
}

pub type ApplyResult = Result<ApplyResponse, ApplyError>;

#[derive(Clone, Debug)]
pub struct Match<'a> {
    pub(crate) phrase: Option<Range>,
    source: &'a str,
    vars: BTreeMap<String, Vec<Range>>,
}

impl<'a> Match<'a> {
    pub(crate) fn new(
        phrase: Option<Range>,
        source: &'a str,
        vars: BTreeMap<String, Vec<Range>>,
    ) -> Match {
        Match {
            phrase,
            source,
            vars,
        }
    }

    pub fn get_string(&self) -> String {
        match self.phrase {
            Some(ref range) => self.range_to_str(range),
            _ => String::new(),
        }
    }

    pub fn get_var(&self, name: &str) -> Option<String> {
        let ranges = self.vars.get(name)?;
        Some(self.range_to_str(&ranges[0]))
    }

    pub fn get_vars(&self, name: &str) -> Vec<String> {
        let ranges = match self.vars.get(name) {
            Some(val) => val,
            _ => return Vec::new(),
        };
        ranges.iter().map(|rng| self.range_to_str(rng)).collect()
    }

    fn range_to_str(&self, range: &Range) -> String {
        let mut chars = self.source.chars().skip(range.begin);
        let mut result = String::new();
        for _ in 0..=range.end - range.begin {
            result.push(chars.next().unwrap())
        }
        result
        // &self.source[range.begin..=range.end]
    }
}

#[derive(Debug)]
pub struct ParseJsonError {
    msg: String,
}

impl ParseJsonError {
    pub(crate) fn new<T>(msg: &str) -> Result<T, ParseJsonError> {
        Err(ParseJsonError {
            msg: msg.to_string(),
        })
    }
    pub(crate) fn invalid_value<T>(
        key: &str,
        expected: &str,
    ) -> Result<T, ParseJsonError> {
        Err(ParseJsonError {
            msg: format!("{} must be {}", key, expected),
        })
    }
    pub(crate) fn not_found<T>(key: &str) -> Result<T, ParseJsonError> {
        Err(ParseJsonError {
            msg: format!("key not found: {}", key),
        })
    }
}

impl fmt::Display for ParseJsonError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.msg.fmt(f)
    }
}

impl Error for ParseJsonError {}

#[derive(Ord, PartialOrd, Eq, PartialEq, Clone, Copy)]
pub enum BeginSymbol {
    Char(char),
    Begin,
    End,
}

pub enum PossibleBegin {
    Symbols(BTreeSet<BeginSymbol>),
    Any,
}

impl PossibleBegin {
    pub fn from_slice(slice: &[BeginSymbol]) -> PossibleBegin {
        PossibleBegin::Symbols(slice.iter().cloned().collect())
    }

    pub fn empty() -> PossibleBegin {
        PossibleBegin::Symbols(BTreeSet::new())
    }

    pub fn join(&self, other: &PossibleBegin) -> PossibleBegin {
        match (self, other) {
            (PossibleBegin::Any, _) | (_, PossibleBegin::Any) => PossibleBegin::Any,
            (PossibleBegin::Symbols(a_set), PossibleBegin::Symbols(b_set)) => {
                PossibleBegin::Symbols(a_set.union(b_set).cloned().collect())
            }
        }
    }

    pub fn has_intersection(&self, other: &PossibleBegin) -> bool {
        match (self, other) {
            (PossibleBegin::Any, _) | (_, PossibleBegin::Any) => true,
            (PossibleBegin::Symbols(a_set), PossibleBegin::Symbols(b_set)) => {
                a_set.intersection(b_set).next().is_some()
            }
        }
    }
}

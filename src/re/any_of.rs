use std::rc::Rc;

use serde_json::{Map, Value};

use crate::entities::re::{
    ApplyError, ApplyResult, ParseJsonError, PossibleBegin, ReType, State,
};
use crate::js_tools::{BoolFieldGetter, FieldGetter, VecFieldGetter};
use crate::proto::builders::IJsonParser;
use crate::proto::into_re::IntoReVec;
use crate::proto::re::{compile_if_required, IRe};
use crate::IntoRe;

const MAIN_JS_KEY: &str = "any_of";
const GREEDY_JS_KEY: &str = "greedy";

pub struct Re {
    variants: Vec<Rc<dyn IRe>>,
    greedy: bool,
}

impl Re {
    pub fn new<T: IntoReVec>(variants: T) -> Re {
        Re {
            variants: variants.into_ire_vec(),
            greedy: false,
        }
    }

    pub fn new_greedy<T: IntoReVec>(variants: T) -> Re {
        Re {
            variants: variants.into_ire_vec(),
            greedy: true,
        }
    }

    pub fn parse_dict(
        parser: &dyn IJsonParser,
        dict: &Map<String, Value>,
    ) -> Result<Option<Rc<dyn IRe>>, ParseJsonError> {
        let opt_variants =
            VecFieldGetter::with_primary_field(dict, MAIN_JS_KEY, &|variants| {
                variants
                    .iter()
                    .map(|child| parser.parse_json(child))
                    .collect::<Result<Vec<Rc<dyn IRe>>, ParseJsonError>>()
            })?;
        let variants = match opt_variants {
            Some(val) => val,
            _ => return Ok(None),
        };
        let greedy = BoolFieldGetter::with_param_field(dict, GREEDY_JS_KEY, &|x| Ok(*x))?;
        Ok(Some(Re { variants, greedy }.into_ire()))
    }

    #[inline(always)]
    fn greedy_match(&self, state: &mut State, begin: usize) -> ApplyResult {
        for re in self.variants.iter() {
            match re.apply(state, begin) {
                Ok(result) => return Ok(result),
                Err(ApplyError::StopSignal) => return Err(ApplyError::StopSignal),
                _ => (),
            }
        }
        Err(ApplyError::NotMatched)
    }

    #[inline(always)]
    fn non_greedy_match(&self, state: &mut State, begin: usize) -> ApplyResult {
        let mut matched = Vec::new();
        for re in self.variants.iter() {
            match re.apply(state, begin) {
                Ok(result) => matched.push(result),
                Err(ApplyError::StopSignal) => return Err(ApplyError::StopSignal),
                _ => (),
            }
        }
        matched.sort_by_key(|res| res.range.as_ref().map(|x| x.end));
        if let Some(val) = matched.last() {
            Ok(val.clone())
        } else {
            Err(ApplyError::NotMatched)
        }
    }

    fn is_child_compatible(&self, other: &Rc<dyn IRe>) -> bool {
        matches!(other.get_type(), ReType::AnyOf) && other.is_greedy() == self.is_greedy()
    }
}

impl IRe for Re {
    fn get_type(&self) -> ReType {
        ReType::AnyOf
    }

    fn apply(&self, state: &mut State, begin: usize) -> ApplyResult {
        if self.greedy {
            self.greedy_match(state, begin)
        } else {
            self.non_greedy_match(state, begin)
        }
    }

    fn show(&self) -> String {
        let joined = self
            .variants
            .iter()
            .map(|x| x.show())
            .collect::<Vec<_>>()
            .join(", ");
        format!(
            "{}({})",
            if self.greedy { "greedy_or" } else { "or" },
            joined
        )
    }

    fn to_json(&self) -> Value {
        let children = Value::Array(self.variants.iter().map(|x| x.to_json()).collect());
        Value::Object(
            vec![
                (MAIN_JS_KEY.to_string(), children),
                (GREEDY_JS_KEY.to_string(), Value::Bool(self.greedy)),
            ]
            .into_iter()
            .collect(),
        )
    }

    fn is_greedy(&self) -> bool {
        self.greedy
    }

    fn get_children(&self) -> Vec<Rc<dyn IRe>> {
        self.variants.clone()
    }

    fn compilation_required(&self) -> bool {
        if self.variants.iter().any(|re| re.is_any()) {
            panic!("'any' can not be variant of 'any_of'")
        }
        self.variants.len() == 1
            || self.variants.iter().any(|re| {
                re.compilation_required() || matches!(re.get_type(), ReType::AnyOf)
            })
    }

    fn possible_begin(&self) -> PossibleBegin {
        self.variants
            .iter()
            .map(|re| re.possible_begin())
            .fold(PossibleBegin::empty(), |a, b| a.join(&b))
    }

    fn compile(&self, next_re: Option<Rc<dyn IRe>>) -> Rc<dyn IRe> {
        if self.variants.len() == 1 {
            return compile_if_required(self.variants[0].clone(), next_re);
        }
        let mut compiled = Vec::new();
        for re in self.variants.iter() {
            let new_re = compile_if_required(re.clone(), next_re.clone());
            if self.is_child_compatible(&new_re) {
                compiled.extend(new_re.get_children().into_iter())
            } else {
                compiled.push(new_re)
            }
        }
        Rc::new(Re {
            variants: compiled,
            greedy: self.greedy,
        })
    }
}

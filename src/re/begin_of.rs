use std::rc::Rc;

use serde_json::{Map, Value};

use crate::entities::re::{
    ApplyError, ApplyResponse, ApplyResult, ParseJsonError, PossibleBegin, ReType, State,
};
use crate::js_tools::{FieldGetter, ValueFieldGetter};
use crate::proto::builders::IJsonParser;
use crate::proto::re::IRe;
use crate::IntoRe;

const PRIMARY_KEY: &str = "begin_of";

pub struct Re {
    re: Rc<dyn IRe>,
}

impl Re {
    pub fn new<T: IntoRe>(re: T) -> Re {
        Re { re: re.into_ire() }
    }

    pub fn parse_dict(
        parser: &dyn IJsonParser,
        dict: &Map<String, Value>,
    ) -> Result<Option<Rc<dyn IRe>>, ParseJsonError> {
        ValueFieldGetter::with_primary_field(dict, PRIMARY_KEY, &|val| {
            let re = parser.parse_json(val)?;
            Ok(Re { re }.into_ire())
        })
    }
}

impl IRe for Re {
    fn get_type(&self) -> ReType {
        ReType::BeginOf
    }

    fn apply(&self, state: &mut State, begin: usize) -> ApplyResult {
        if self.re.apply(state, begin).is_ok() {
            Ok(ApplyResponse::new_empty(begin))
        } else {
            Err(ApplyError::NotMatched)
        }
    }

    fn show(&self) -> String {
        format!("begin_of({})", self.re.show())
    }

    fn to_json(&self) -> Value {
        Value::Object(
            vec![(PRIMARY_KEY.to_string(), self.re.to_json())]
                .into_iter()
                .collect(),
        )
    }

    fn possible_begin(&self) -> PossibleBegin {
        self.re.possible_begin()
    }
}

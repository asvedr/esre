use std::rc::Rc;

use serde_json::{Map, Value};

use crate::entities::re::{
    ApplyError, ApplyResponse, ApplyResult, BeginSymbol, ParseJsonError, PossibleBegin,
    ReType, State,
};
use crate::js_tools::{FieldGetter, StrFieldGetter};
use crate::proto::builders::IJsonParser;
use crate::proto::re::IRe;
use crate::IntoRe;

const PRIMARY_KEY: &str = "match";

pub struct Re {
    template: Vec<char>,
}

impl Re {
    pub fn new(source: &str) -> Re {
        Re {
            template: source.chars().collect(),
        }
    }

    pub fn from_char(source: char) -> Re {
        Re {
            template: vec![source],
        }
    }

    pub fn parse_dict(
        _parser: &dyn IJsonParser,
        dict: &Map<String, Value>,
    ) -> Result<Option<Rc<dyn IRe>>, ParseJsonError> {
        StrFieldGetter::with_primary_field(dict, PRIMARY_KEY, &|val| {
            let template = val.chars().collect::<Vec<_>>();
            Ok(Re { template }.into_ire())
        })
    }
}

impl IRe for Re {
    fn get_type(&self) -> ReType {
        ReType::MatchStr
    }

    fn apply(&self, state: &mut State, begin: usize) -> ApplyResult {
        let mut end = begin;
        for local_index in 0..self.template.len() {
            let global_index = local_index + begin;
            if global_index >= state.text.len() {
                return Err(ApplyError::NotMatched);
            }
            let expected = self.template[local_index];
            let got = state.text[global_index];
            if expected != got {
                return Err(ApplyError::NotMatched);
            };
            end = global_index;
        }
        Ok(ApplyResponse::new_range(end + 1, begin, end))
    }

    fn show(&self) -> String {
        let mut result = String::new();
        for c in self.template.iter() {
            result.push(*c);
        }
        format!("{:?}", result)
    }

    fn to_json(&self) -> Value {
        let mut template = String::new();
        for c in self.template.iter() {
            template.push(*c)
        }
        Value::Object(
            vec![(PRIMARY_KEY.to_string(), Value::String(template))]
                .into_iter()
                .collect(),
        )
    }

    fn possible_begin(&self) -> PossibleBegin {
        PossibleBegin::from_slice(&[BeginSymbol::Char(self.template[0])])
    }
}

use std::mem;
use std::rc::Rc;

use serde_json::value::to_value;
use serde_json::{Map, Value};

use crate::entities::re::{
    ApplyError, ApplyResponse, ApplyResult, ParseJsonError, PossibleBegin, ReType, State,
};
use crate::js_tools::{FieldGetter, UsizeFieldGetter, ValueFieldGetter};
use crate::proto::builders::IJsonParser;
use crate::proto::into_re::IntoRe;
use crate::proto::re::{compile_if_required, IRe};

const PRIMARY_KEY: &str = "repeat";
const MIN_KEY: &str = "min";
const MAX_KEY: &str = "max";

pub struct Re {
    re: Rc<dyn IRe>,
    min: Option<usize>,
    max: Option<usize>,
    next_re: Option<Rc<dyn IRe>>,
}

struct LoopState {
    begin: usize,
    count: usize,
    result: Option<ApplyResponse>,
    stop_flag: bool,
}

impl Re {
    pub fn one_or_more<T: IntoRe>(re: T) -> Re {
        Re {
            re: re.into_ire(),
            min: Some(1),
            max: None,
            next_re: None,
        }
    }

    pub fn zero_or_more<T: IntoRe>(re: T) -> Re {
        Re {
            re: re.into_ire(),
            min: None,
            max: None,
            next_re: None,
        }
    }

    pub fn custom<T: IntoRe>(re: T, min: Option<usize>, max: Option<usize>) -> Re {
        Re {
            re: re.into_ire(),
            min,
            max,
            next_re: None,
        }
    }

    pub fn parse_dict(
        parser: &dyn IJsonParser,
        dict: &Map<String, Value>,
    ) -> Result<Option<Rc<dyn IRe>>, ParseJsonError> {
        let opt_re = ValueFieldGetter::with_primary_field(dict, PRIMARY_KEY, &|val| {
            parser.parse_json(val)
        })?;
        let re = match opt_re {
            Some(val) => val,
            _ => return Ok(None),
        };
        let min = UsizeFieldGetter::with_param_nullable_field(dict, MIN_KEY, &|val| {
            Ok(val.cloned())
        })?;
        let max = UsizeFieldGetter::with_param_nullable_field(dict, MAX_KEY, &|val| {
            Ok(val.cloned())
        })?;
        Ok(Some(
            Re {
                re,
                min,
                max,
                next_re: None,
            }
            .into_ire(),
        ))
    }

    fn try_finalize_seq(
        &self,
        accum: Option<ApplyResponse>,
        count: usize,
    ) -> Result<ApplyResponse, ApplyError> {
        match self.min {
            Some(ref val) if count < *val => return Err(ApplyError::NotMatched),
            _ => (),
        };
        if count == 0 {
            return Err(ApplyError::Empty);
        }
        Ok(accum.unwrap())
    }

    fn apply_iteration(
        &self,
        global_state: &mut State,
        loop_state: &mut LoopState,
    ) -> Result<(), ApplyError> {
        match self.re.apply(global_state, loop_state.begin) {
            Ok(applied) => {
                let begin = applied.index;
                let acc = mem::replace(&mut loop_state.result, None);
                let joined = ApplyResponse::join_acc(acc, applied);
                loop_state.result = Some(joined);
                loop_state.count += 1;
                loop_state.begin = begin;
                Ok(())
            }
            Err(ApplyError::NotMatched) => {
                let acc = mem::replace(&mut loop_state.result, None);
                let result = self.try_finalize_seq(acc, loop_state.count)?;
                loop_state.result = Some(result);
                loop_state.stop_flag = true;
                Ok(())
            }
            Err(err) => Err(err),
        }
    }

    fn try_next_re(&self, state: &mut State, begin: usize) -> bool {
        if let Some(ref re) = self.next_re {
            re.apply(state, begin).is_ok()
        } else {
            false
        }
    }
}

impl IRe for Re {
    fn get_type(&self) -> ReType {
        ReType::Repeat
    }

    fn apply(&self, global_state: &mut State, begin: usize) -> ApplyResult {
        let min_count = self.min.unwrap_or(0);
        let mut loop_state = LoopState {
            begin,
            count: 0,
            result: None,
            stop_flag: false,
        };
        while Some(loop_state.count) != self.max {
            if loop_state.count >= min_count
                && self.next_re.is_some()
                && self.try_next_re(global_state, loop_state.begin)
            {
                return self.try_finalize_seq(loop_state.result, loop_state.count);
            }
            self.apply_iteration(global_state, &mut loop_state)?;
            if loop_state.stop_flag {
                return Ok(loop_state.result.unwrap());
            }
        }
        Ok(loop_state.result.unwrap())
    }

    fn show(&self) -> String {
        format!(
            "{}{},{}>{},{}{}",
            '{',
            self.re.show(),
            self.min
                .as_ref()
                .map_or("_".to_string(), |x| { x.to_string() }),
            self.max
                .as_ref()
                .map_or("_".to_string(), |x| { x.to_string() }),
            self.next_re.is_some(),
            '}'
        )
    }

    fn to_json(&self) -> Value {
        Value::Object(
            vec![
                ("repeat".to_string(), self.re.to_json()),
                ("min".to_string(), to_value(self.min).unwrap()),
                ("max".to_string(), to_value(self.max).unwrap()),
            ]
            .into_iter()
            .collect(),
        )
    }

    fn is_any(&self) -> bool {
        self.re.is_any()
    }

    fn get_children(&self) -> Vec<Rc<dyn IRe>> {
        vec![self.re.clone()]
    }

    fn compilation_required(&self) -> bool {
        if matches!(self.re.get_type(), ReType::Repeat) {
            panic!("Repeat is direct child of repeat")
        }
        self.re.compilation_required() || self.max.is_none()
    }

    fn possible_begin(&self) -> PossibleBegin {
        if self.min.unwrap_or(0) > 0 {
            self.re.possible_begin()
        } else {
            PossibleBegin::Any
        }
    }

    fn compile(&self, mut next_re: Option<Rc<dyn IRe>>) -> Rc<dyn IRe> {
        let re = compile_if_required(self.re.clone(), next_re.clone());
        let probe_required = if let Some(ref next) = next_re {
            re.possible_begin().has_intersection(&next.possible_begin())
        } else {
            false
        };
        if self.max.is_some() || !probe_required {
            next_re = None;
        }
        Re {
            re,
            next_re,
            min: self.min,
            max: self.max,
        }
        .into_ire()
    }
}

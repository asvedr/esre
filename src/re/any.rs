use serde_json::Value;

use crate::entities::re::{
    ApplyError, ApplyResponse, ApplyResult, PossibleBegin, ReType, State,
};
use crate::proto::re::IRe;

pub struct Re {}

impl Re {
    pub fn new() -> Re {
        Re {}
    }
}

impl IRe for Re {
    fn get_type(&self) -> ReType {
        ReType::Any
    }

    fn apply(&self, state: &mut State, begin: usize) -> ApplyResult {
        if begin < state.text.len() {
            Ok(ApplyResponse::new_range(begin + 1, begin, begin))
        } else {
            Err(ApplyError::NotMatched)
        }
    }

    fn show(&self) -> String {
        ".".to_string()
    }

    fn to_json(&self) -> Value {
        Value::String("any".to_string())
    }

    fn is_any(&self) -> bool {
        true
    }

    fn possible_begin(&self) -> PossibleBegin {
        PossibleBegin::Any
    }
}

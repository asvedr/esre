use std::rc::Rc;

use serde_json::{to_value, Map, Value};

use crate::entities::re::{
    ApplyError, ApplyResponse, ApplyResult, BeginSymbol, ParseJsonError, PossibleBegin,
    ReType, State,
};
use crate::js_tools::{FieldGetter, VecFieldGetter};
use crate::proto::builders::IJsonParser;
use crate::proto::into_char_ranges::IntoCharRanges;
use crate::proto::re::IRe;
use crate::IntoRe;

const PRIMARY_KEY: &str = "char_range";

pub struct Re {
    ranges: Vec<(char, char)>,
}

fn parse_range(value: &Value) -> Result<(char, char), ParseJsonError> {
    let pair = match value {
        Value::Array(val) if val.len() == 2 => val,
        _ => return ParseJsonError::invalid_value(PRIMARY_KEY, "list of (char, char)"),
    };
    match (&pair[0], &pair[1]) {
        (Value::String(ref f), Value::String(ref t)) => {
            Ok((f.chars().next().unwrap(), t.chars().next().unwrap()))
        }
        _ => ParseJsonError::invalid_value(PRIMARY_KEY, "list of (char, char)"),
    }
}

impl Re {
    pub fn new<T: IntoCharRanges>(src: T) -> Re {
        Re {
            ranges: src.into_char_ranges(),
        }
    }

    pub fn parse_dict(
        _parser: &dyn IJsonParser,
        dict: &Map<String, Value>,
    ) -> Result<Option<Rc<dyn IRe>>, ParseJsonError> {
        let opt_ranges =
            VecFieldGetter::with_primary_field(dict, PRIMARY_KEY, &|variants| {
                variants
                    .iter()
                    .map(parse_range)
                    .collect::<Result<Vec<(char, char)>, ParseJsonError>>()
            })?;
        let ranges = match opt_ranges {
            Some(val) => val,
            _ => return Ok(None),
        };
        Ok(Some(Re { ranges }.into_ire()))
    }

    fn check_sym(&self, sym: char) -> bool {
        for (begin, end) in self.ranges.iter() {
            if sym >= *begin && sym <= *end {
                return true;
            }
        }
        false
    }
}

impl IRe for Re {
    fn get_type(&self) -> ReType {
        ReType::CharRange
    }

    fn apply(&self, state: &mut State, begin: usize) -> ApplyResult {
        if begin >= state.text.len() {
            return Err(ApplyError::NotMatched);
        }
        let got = state.text[begin];
        if !self.check_sym(got) {
            return Err(ApplyError::NotMatched);
        }
        Ok(ApplyResponse::new_range(begin + 1, begin, begin))
    }

    fn show(&self) -> String {
        let joined = self
            .ranges
            .iter()
            .map(|(a, b)| format!("'{}'-'{}'", a, b))
            .collect::<Vec<_>>()
            .join(",");
        format!("[{}]", joined)
    }

    fn to_json(&self) -> Value {
        let ranges = self
            .ranges
            .iter()
            .map(|rng| to_value(rng).unwrap())
            .collect::<Vec<_>>();
        Value::Object(
            vec![(PRIMARY_KEY.to_string(), Value::Array(ranges))]
                .into_iter()
                .collect(),
        )
    }

    fn possible_begin(&self) -> PossibleBegin {
        let mut symbols = Vec::new();
        for (begin, end) in self.ranges.iter() {
            for symbol in *begin..=*end {
                symbols.push(BeginSymbol::Char(symbol))
            }
        }
        PossibleBegin::from_slice(&symbols)
    }
}

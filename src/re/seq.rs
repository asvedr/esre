use std::rc::Rc;

use serde_json::{Map, Value};

use crate::entities::re::{
    ApplyError, ApplyResponse, ApplyResult, ParseJsonError, PossibleBegin, ReType, State,
};
use crate::js_tools::{FieldGetter, VecFieldGetter};
use crate::proto::builders::IJsonParser;
use crate::proto::into_re::{IntoRe, IntoReVec};
use crate::proto::re::{compile_if_required, IRe};

const PRIMARY_KEY: &str = "seq";

pub struct Re {
    re_vec: Vec<Rc<dyn IRe>>,
}

impl Re {
    pub fn new<T: IntoReVec>(re_vec: T) -> Re {
        Re {
            re_vec: re_vec.into_ire_vec(),
        }
    }

    pub fn new_ire<T: IntoReVec>(re_vec: T) -> Rc<dyn IRe> {
        let mut vec = re_vec.into_ire_vec();
        if vec.len() == 1 {
            return vec.remove(0);
        }
        Re { re_vec: vec }.into_ire()
    }

    pub fn parse_dict(
        parser: &dyn IJsonParser,
        dict: &Map<String, Value>,
    ) -> Result<Option<Rc<dyn IRe>>, ParseJsonError> {
        VecFieldGetter::with_primary_field(dict, PRIMARY_KEY, &|values| {
            let re_vec = values
                .iter()
                .map(|value| parser.parse_json(value))
                .collect::<Result<Vec<_>, _>>()?;
            Ok(Re { re_vec }.into_ire())
        })
    }
}

impl IRe for Re {
    fn get_type(&self) -> ReType {
        ReType::Seq
    }

    fn apply(&self, state: &mut State, mut begin: usize) -> ApplyResult {
        let mut result = None;
        for re in self.re_vec.iter() {
            match re.apply(state, begin) {
                Ok(applied) => {
                    begin = applied.index;
                    result = Some(ApplyResponse::join_acc(result, applied));
                }
                Err(ApplyError::Empty) => (),
                Err(err) => return Err(err),
            }
        }
        match result {
            Some(val) => Ok(val),
            _ => Err(ApplyError::Empty),
        }
    }

    fn show(&self) -> String {
        let joined = self
            .re_vec
            .iter()
            .map(|x| x.show())
            .collect::<Vec<_>>()
            .join(", ");
        format!("seq({})", joined)
    }

    fn to_json(&self) -> Value {
        let children = Value::Array(self.re_vec.iter().map(|x| x.to_json()).collect());
        Value::Object(
            vec![(PRIMARY_KEY.to_string(), children)]
                .into_iter()
                .collect(),
        )
    }

    fn get_children(&self) -> Vec<Rc<dyn IRe>> {
        self.re_vec.clone()
    }

    fn compilation_required(&self) -> bool {
        self.re_vec.len() == 1
            || self.re_vec.iter().any(|re| {
                re.compilation_required() || matches!(re.get_type(), ReType::Seq)
            })
    }

    fn possible_begin(&self) -> PossibleBegin {
        self.re_vec[0].possible_begin()
    }

    fn compile(&self, mut next_re: Option<Rc<dyn IRe>>) -> Rc<dyn IRe> {
        if self.re_vec.len() == 1 {
            return compile_if_required(self.re_vec[0].clone(), next_re);
        }
        let mut compiled: Vec<Rc<dyn IRe>> = Vec::new();
        for source in self.re_vec.iter().rev() {
            let new_re = compile_if_required(source.clone(), next_re);
            if matches!(new_re.get_type(), ReType::Seq) {
                compiled.extend(new_re.get_children().into_iter().rev());
                next_re = compiled.last().cloned();
            } else {
                compiled.push(new_re.clone());
                next_re = Some(new_re);
            }
        }
        compiled.reverse();
        Re::new_ire(compiled)
    }
}

use std::rc::Rc;

use serde_json::{Map, Value};

use crate::entities::re::{
    ApplyError, ApplyResult, ParseJsonError, PossibleBegin, ReType, State,
};
use crate::js_tools::{FieldGetter, ValueFieldGetter};
use crate::proto::builders::IJsonParser;
use crate::proto::into_re::IntoRe;
use crate::proto::re::{compile_if_required, IRe};

const PRIMARY_KEY: &str = "opt";

pub struct Re {
    re: Rc<dyn IRe>,
    next_re: Option<Rc<dyn IRe>>,
}

impl Re {
    pub fn new<T: IntoRe>(re: T) -> Re {
        Re {
            re: re.into_ire(),
            next_re: None,
        }
    }

    pub fn parse_dict(
        parser: &dyn IJsonParser,
        dict: &Map<String, Value>,
    ) -> Result<Option<Rc<dyn IRe>>, ParseJsonError> {
        ValueFieldGetter::with_primary_field(dict, PRIMARY_KEY, &|val| {
            let re = parser.parse_json(val)?;
            Ok(Re { re, next_re: None }.into_ire())
        })
    }
}

impl IRe for Re {
    fn get_type(&self) -> ReType {
        ReType::Opt
    }

    fn apply(&self, state: &mut State, begin: usize) -> ApplyResult {
        if let Some(ref re) = self.next_re {
            if re.apply(state, begin).is_ok() {
                return Err(ApplyError::Empty);
            }
        }
        match self.re.apply(state, begin) {
            Ok(val) => Ok(val),
            Err(ApplyError::StopSignal) => Err(ApplyError::StopSignal),
            _ => Err(ApplyError::Empty),
        }
    }

    fn show(&self) -> String {
        format!("opt({})", self.re.show())
    }

    fn to_json(&self) -> Value {
        Value::Object(
            vec![(PRIMARY_KEY.to_string(), self.re.to_json())]
                .into_iter()
                .collect(),
        )
    }

    fn is_any(&self) -> bool {
        self.re.is_any()
    }

    fn get_children(&self) -> Vec<Rc<dyn IRe>> {
        vec![self.re.clone()]
    }

    fn compilation_required(&self) -> bool {
        true
    }

    fn possible_begin(&self) -> PossibleBegin {
        self.re.possible_begin()
    }

    fn compile(&self, next_re: Option<Rc<dyn IRe>>) -> Rc<dyn IRe> {
        let re = compile_if_required(self.re.clone(), next_re.clone());
        Rc::new(Re { re, next_re })
    }
}

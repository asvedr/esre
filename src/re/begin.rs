use serde_json::Value;

use crate::entities::re::{
    ApplyError, ApplyResponse, ApplyResult, BeginSymbol, PossibleBegin, ReType, State,
};
use crate::proto::re::IRe;

pub struct Re {}

impl Re {
    pub fn new() -> Re {
        Re {}
    }
}

impl IRe for Re {
    fn get_type(&self) -> ReType {
        ReType::Begin
    }

    fn apply(&self, _: &mut State, begin: usize) -> ApplyResult {
        if begin == 0 {
            Ok(ApplyResponse::new_empty(0))
        } else {
            Err(ApplyError::NotMatched)
        }
    }

    fn show(&self) -> String {
        "^".to_string()
    }

    fn to_json(&self) -> Value {
        Value::String("begin".to_string())
    }

    fn possible_begin(&self) -> PossibleBegin {
        PossibleBegin::from_slice(&[BeginSymbol::Begin])
    }
}

use std::rc::Rc;

use serde_json::{Map, Value};

use crate::entities::re::{ApplyResult, ParseJsonError, PossibleBegin, ReType, State};
use crate::js_tools::{BoolFieldGetter, FieldGetter, StrFieldGetter, ValueFieldGetter};
use crate::proto::builders::IJsonParser;
use crate::proto::into_re::IntoRe;
use crate::proto::re::{compile_if_required, is_any, IRe};

const PRIMARY_KEY: &str = "var";
const NAME_KEY: &str = "name";
const AS_LABEL_KEY: &str = "as_label";

pub struct Re {
    re: Rc<dyn IRe>,
    name: String,
    as_label: bool,
}

pub struct CompiledWithLabel {
    re: Re,
}

pub struct CompiledNoLabel {
    re: Re,
}

impl Re {
    pub fn new<T: IntoRe>(name: String, re: T, as_label: bool) -> Re {
        Re {
            name,
            re: re.into_ire(),
            as_label,
        }
    }

    pub fn parse_dict(
        parser: &dyn IJsonParser,
        dict: &Map<String, Value>,
    ) -> Result<Option<Rc<dyn IRe>>, ParseJsonError> {
        let opt_child =
            ValueFieldGetter::with_primary_field(dict, PRIMARY_KEY, &|val| {
                parser.parse_json(val)
            })?;
        let re = match opt_child {
            Some(val) => val,
            _ => return Ok(None),
        };
        let name =
            StrFieldGetter::with_param_field(dict, NAME_KEY, &|val| Ok(val.clone()))?;
        let as_label =
            BoolFieldGetter::with_param_field(dict, AS_LABEL_KEY, &|val| Ok(*val))?;
        Ok(Some(Re { re, name, as_label }.into_ire()))
    }

    fn contains_var(&self) -> bool {
        is_any(self.re.clone(), &|re| re.get_type() == ReType::Var)
    }

    fn with_new_child(&self, re: Rc<dyn IRe>) -> Re {
        Re {
            re,
            name: self.name.clone(),
            as_label: self.as_label,
        }
    }
}

impl IRe for Re {
    fn get_type(&self) -> ReType {
        ReType::Var
    }

    fn get_value_type(&self) -> ReType {
        self.re.get_value_type()
    }

    fn apply(&self, _: &mut State, _: usize) -> ApplyResult {
        unimplemented!("can not be called directly")
    }

    fn show(&self) -> String {
        format!(
            "var(\"{}\", {}, {})",
            self.name,
            self.as_label,
            self.re.show()
        )
    }

    fn to_json(&self) -> Value {
        Value::Object(
            vec![
                (PRIMARY_KEY.to_string(), self.re.to_json()),
                (NAME_KEY.to_string(), Value::String(self.name.clone())),
                (AS_LABEL_KEY.to_string(), Value::Bool(self.as_label)),
            ]
            .into_iter()
            .collect(),
        )
    }

    fn is_any(&self) -> bool {
        self.re.is_any()
    }

    fn get_children(&self) -> Vec<Rc<dyn IRe>> {
        vec![self.re.clone()]
    }

    fn compilation_required(&self) -> bool {
        true
    }

    fn compile(&self, next_re: Option<Rc<dyn IRe>>) -> Rc<dyn IRe> {
        let child = compile_if_required(self.re.clone(), next_re);
        if !self.as_label || !self.contains_var() {
            CompiledNoLabel {
                re: self.with_new_child(child),
            }
            .into_ire()
        } else {
            CompiledWithLabel {
                re: self.with_new_child(child),
            }
            .into_ire()
        }
    }

    fn possible_begin(&self) -> PossibleBegin {
        self.re.possible_begin()
    }
}

impl IRe for CompiledNoLabel {
    fn get_type(&self) -> ReType {
        self.re.get_type()
    }

    fn apply(&self, state: &mut State, begin: usize) -> ApplyResult {
        let mut result = self.re.re.apply(state, begin)?;
        let name = self.re.name.clone();
        if let Some(range) = result.range {
            result = result.add_var(&state.var_layer, name, range)
        }
        Ok(result)
    }

    fn show(&self) -> String {
        format!("nolab_{}", self.re.show())
    }

    fn to_json(&self) -> Value {
        self.re.to_json()
    }

    fn possible_begin(&self) -> PossibleBegin {
        self.re.possible_begin()
    }
}

impl IRe for CompiledWithLabel {
    fn get_type(&self) -> ReType {
        self.re.get_type()
    }

    fn apply(&self, state: &mut State, begin: usize) -> ApplyResult {
        state.var_layer.push(self.re.name.clone());
        let child_result = self.re.re.apply(state, begin);
        state.var_layer.pop();
        let mut result = child_result?;
        let name = self.re.name.clone();
        if let Some(range) = result.range {
            result = result.add_var(&state.var_layer, name, range)
        }
        Ok(result)
    }

    fn show(&self) -> String {
        format!("lab_{}", self.re.show())
    }

    fn to_json(&self) -> Value {
        self.re.to_json()
    }

    fn possible_begin(&self) -> PossibleBegin {
        self.re.possible_begin()
    }
}

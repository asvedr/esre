use std::rc::Rc;

use serde_json::{Map, Value};

use crate::entities::re::{ApplyResult, ParseJsonError, PossibleBegin, ReType, State};
use crate::js_tools::{FieldGetter, StrFieldGetter, ValueFieldGetter};
use crate::proto::builders::IJsonParser;
use crate::proto::into_re::IntoRe;
use crate::proto::re::IRe;

const PRIMARY_KEY: &str = "label";
const NAME_KEY: &str = "name";

pub struct Re {
    re: Rc<dyn IRe>,
    name: String,
}

impl Re {
    pub fn new<T: IntoRe>(name: String, re: T) -> Re {
        Re {
            name,
            re: re.into_ire(),
        }
    }

    pub fn parse_dict(
        parser: &dyn IJsonParser,
        dict: &Map<String, Value>,
    ) -> Result<Option<Rc<dyn IRe>>, ParseJsonError> {
        let opt_child =
            ValueFieldGetter::with_primary_field(dict, PRIMARY_KEY, &|val| {
                parser.parse_json(val)
            })?;
        let re = match opt_child {
            Some(val) => val,
            _ => return Ok(None),
        };
        let name =
            StrFieldGetter::with_param_field(dict, NAME_KEY, &|val| Ok(val.clone()))?;
        Ok(Some(Re { re, name }.into_ire()))
    }
}

impl IRe for Re {
    fn get_type(&self) -> ReType {
        ReType::Var
    }

    fn get_value_type(&self) -> ReType {
        self.re.get_value_type()
    }

    fn apply(&self, state: &mut State, begin: usize) -> ApplyResult {
        state.var_layer.push(self.name.clone());
        let result = self.re.apply(state, begin);
        state.var_layer.pop();
        result
    }

    fn show(&self) -> String {
        format!("label(\"{}\", {})", self.name, self.re.show())
    }

    fn to_json(&self) -> Value {
        Value::Object(
            vec![
                (PRIMARY_KEY.to_string(), self.re.to_json()),
                (NAME_KEY.to_string(), Value::String(self.name.clone())),
            ]
            .into_iter()
            .collect(),
        )
    }

    fn is_any(&self) -> bool {
        self.re.is_any()
    }

    fn get_children(&self) -> Vec<Rc<dyn IRe>> {
        vec![self.re.clone()]
    }

    fn compilation_required(&self) -> bool {
        self.re.compilation_required()
    }

    fn possible_begin(&self) -> PossibleBegin {
        self.re.possible_begin()
    }

    fn compile(&self, next_re: Option<Rc<dyn IRe>>) -> Rc<dyn IRe> {
        Rc::new(Re {
            name: self.name.clone(),
            re: self.re.compile(next_re),
        })
    }
}

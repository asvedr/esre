use std::rc::Rc;
use std::str::FromStr;

use serde_json::{Map, Value};

use crate::entities::re::{
    ApplyError, ApplyResult, ParseJsonError, PossibleBegin, ReType, State,
};
use crate::js_tools::{FieldGetter, StrFieldGetter, ValueFieldGetter};
use crate::proto::builders::IJsonParser;
use crate::proto::into_re::IntoRe;
use crate::proto::re::IRe;

const PRIMARY_KEY: &str = "not";
const RAISE_KEY: &str = "raise";

pub struct Re {
    re: Rc<dyn IRe>,
    raise: ApplyError,
}

impl Re {
    pub fn new<T: IntoRe>(re: T, raise: ApplyError) -> Re {
        Re {
            re: re.into_ire(),
            raise,
        }
    }

    pub fn parse_dict(
        parser: &dyn IJsonParser,
        dict: &Map<String, Value>,
    ) -> Result<Option<Rc<dyn IRe>>, ParseJsonError> {
        let opt_re = ValueFieldGetter::with_primary_field(dict, PRIMARY_KEY, &|val| {
            parser.parse_json(val)
        })?;
        let re = match opt_re {
            Some(val) => val,
            _ => return Ok(None),
        };
        let raise = StrFieldGetter::with_param_field(dict, RAISE_KEY, &|val| {
            if let Ok(val) = ApplyError::from_str(val) {
                Ok(val)
            } else {
                ParseJsonError::new("invalid raise value")
            }
        })?;
        Ok(Some(Re { re, raise }.into_ire()))
    }
}

impl IRe for Re {
    fn get_type(&self) -> ReType {
        ReType::Not
    }

    fn apply(&self, state: &mut State, begin: usize) -> ApplyResult {
        let child_result = self.re.apply(state, begin);
        if child_result.is_ok() {
            Err(self.raise)
        } else {
            Err(ApplyError::Empty)
        }
    }

    fn show(&self) -> String {
        format!("not({}, {})", self.re.show(), self.raise.to_string())
    }

    fn to_json(&self) -> Value {
        Value::Object(
            vec![
                (PRIMARY_KEY.to_string(), self.re.to_json()),
                (RAISE_KEY.to_string(), Value::String(self.raise.to_string())),
            ]
            .into_iter()
            .collect(),
        )
    }

    fn is_any(&self) -> bool {
        self.re.is_any()
    }

    fn get_children(&self) -> Vec<Rc<dyn IRe>> {
        vec![self.re.clone()]
    }

    fn compilation_required(&self) -> bool {
        self.re.compilation_required()
    }

    fn possible_begin(&self) -> PossibleBegin {
        PossibleBegin::Any
    }

    fn compile(&self, next_re: Option<Rc<dyn IRe>>) -> Rc<dyn IRe> {
        Rc::new(Re {
            re: self.re.compile(next_re),
            raise: self.raise,
        })
    }
}

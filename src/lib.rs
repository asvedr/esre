#![allow(clippy::many_single_char_names)]
mod builders;
mod entities;
mod js_tools;
mod proto;
mod re;
#[cfg(test)]
mod tests;

pub use builders::{Re, ReCompiled, ReProgress};
pub use entities::re::Match;
pub use proto::into_char_ranges::IntoCharRanges;
pub use proto::into_re::{IntoRe, IntoReVec};
pub use proto::re::IRe;

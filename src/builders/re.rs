use std::rc::Rc;

use serde_json::Value;

use crate::builders::json_parser::Parser;
use crate::builders::ReCompiled;
use crate::entities::re::{ApplyError, ParseJsonError};
use crate::proto::builders::IJsonParser;
use crate::proto::into_char_ranges::IntoCharRanges;
use crate::proto::into_re::{IntoRe, IntoReVec};
use crate::proto::re::IRe;
use crate::re;

pub struct Re {}

#[derive(Clone)]
pub struct ReProgress {
    seq: Vec<Rc<dyn IRe>>,
}

impl Re {
    pub fn parse_json(json: &Value) -> Result<ReProgress, ParseJsonError> {
        let re = Parser::new().parse_json(json)?;
        Ok(ReProgress { seq: vec![re] })
    }

    pub fn create<T: IntoReVec>(val: T) -> ReProgress {
        ReProgress {
            seq: val.into_ire_vec(),
        }
    }

    pub fn any() -> ReProgress {
        ReProgress {
            seq: re::any::Re::new().into_ire_vec(),
        }
    }

    pub fn one_or_more<T: IntoRe>(val: T) -> ReProgress {
        ReProgress {
            seq: re::repeat::Re::one_or_more(val).into_ire_vec(),
        }
    }

    pub fn zero_or_more<T: IntoRe>(val: T) -> ReProgress {
        ReProgress {
            seq: re::repeat::Re::zero_or_more(val).into_ire_vec(),
        }
    }

    pub fn repeat<T: IntoRe>(
        min: Option<usize>,
        max: Option<usize>,
        val: T,
    ) -> ReProgress {
        ReProgress {
            seq: re::repeat::Re::custom(val, min, max).into_ire_vec(),
        }
    }

    pub fn ranges<T: IntoCharRanges>(ranges: T) -> ReProgress {
        ReProgress {
            seq: re::char_range::Re::new(ranges).into_ire_vec(),
        }
    }

    pub fn not<T: IntoRe>(val: T) -> ReProgress {
        ReProgress {
            seq: re::not::Re::new(val, ApplyError::NotMatched).into_ire_vec(),
        }
    }

    pub fn stop_all_if<T: IntoRe>(val: T) -> ReProgress {
        ReProgress {
            seq: re::not::Re::new(val, ApplyError::StopSignal).into_ire_vec(),
        }
    }

    pub fn opt<T: IntoRe>(val: T) -> ReProgress {
        ReProgress {
            seq: re::opt::Re::new(val).into_ire_vec(),
        }
    }

    pub fn var<T: IntoRe>(name: &str, val: T) -> ReProgress {
        ReProgress {
            seq: re::var::Re::new(name.to_string(), val, false).into_ire_vec(),
        }
    }

    pub fn var_label<T: IntoRe>(name: &str, val: T) -> ReProgress {
        ReProgress {
            seq: re::var::Re::new(name.to_string(), val, true).into_ire_vec(),
        }
    }

    pub fn label<T: IntoRe>(name: &str, val: T) -> ReProgress {
        ReProgress {
            seq: re::label::Re::new(name.to_string(), val).into_ire_vec(),
        }
    }

    pub fn any_of<T: IntoReVec>(val: T) -> ReProgress {
        ReProgress {
            seq: re::any_of::Re::new(val).into_ire_vec(),
        }
    }

    pub fn any_of_greedy<T: IntoReVec>(val: T) -> ReProgress {
        ReProgress {
            seq: re::any_of::Re::new_greedy(val).into_ire_vec(),
        }
    }

    pub fn begin() -> ReProgress {
        ReProgress {
            seq: re::begin::Re::new().into_ire_vec(),
        }
    }

    pub fn begin_of<T: IntoRe>(re: T) -> ReProgress {
        ReProgress {
            seq: re::begin_of::Re::new(re).into_ire_vec(),
        }
    }

    pub fn end() -> ReProgress {
        ReProgress {
            seq: re::end::Re::new().into_ire_vec(),
        }
    }
}

impl ReProgress {
    pub fn join<T: IntoReVec>(mut self, val: T) -> ReProgress {
        self.seq.extend(val.into_ire_vec().into_iter());
        self
    }

    // JOIN OPTIONS
    pub fn any(self) -> ReProgress {
        self.join(Re::any())
    }

    pub fn one_or_more<T: IntoRe>(self, val: T) -> ReProgress {
        self.join(Re::one_or_more(val))
    }

    pub fn zero_or_more<T: IntoRe>(self, val: T) -> ReProgress {
        self.join(Re::zero_or_more(val))
    }

    pub fn repeat<T: IntoRe>(
        self,
        min: Option<usize>,
        max: Option<usize>,
        val: T,
    ) -> ReProgress {
        self.join(Re::repeat(min, max, val))
    }

    pub fn ranges<T: IntoCharRanges>(self, ranges: T) -> ReProgress {
        self.join(Re::ranges(ranges))
    }

    pub fn not<T: IntoRe>(self, val: T) -> ReProgress {
        self.join(Re::not(val))
    }

    pub fn stop_all_if<T: IntoRe>(self, val: T) -> ReProgress {
        self.join(Re::stop_all_if(val))
    }

    pub fn opt<T: IntoRe>(self, val: T) -> ReProgress {
        self.join(Re::opt(val))
    }

    pub fn var<T: IntoRe>(self, name: &str, val: T) -> ReProgress {
        self.join(Re::var(name, val))
    }

    pub fn var_label<T: IntoRe>(self, name: &str, val: T) -> ReProgress {
        self.join(Re::var_label(name, val))
    }

    pub fn label<T: IntoRe>(self, name: &str, val: T) -> ReProgress {
        self.join(Re::label(name, val))
    }

    pub fn any_of<T: IntoReVec>(self, val: T) -> ReProgress {
        self.join(Re::any_of(val))
    }

    pub fn any_of_greedy<T: IntoReVec>(self, val: T) -> ReProgress {
        self.join(Re::any_of_greedy(val))
    }

    pub fn end(self) -> ReProgress {
        self.join(Re::end())
    }

    pub fn begin_of<T: IntoRe>(self, val: T) -> ReProgress {
        self.join(Re::begin_of(val))
    }

    // COLLAPSE OPTIONS
    pub fn into_one_or_more(self) -> ReProgress {
        Re::one_or_more(self.make_seq())
    }

    pub fn into_zero_or_more(self) -> ReProgress {
        Re::zero_or_more(self.make_seq())
    }

    pub fn into_repeat(self, min: Option<usize>, max: Option<usize>) -> ReProgress {
        Re::repeat(min, max, self.make_seq())
    }

    pub fn into_not(self) -> ReProgress {
        Re::not(self.make_seq())
    }

    pub fn into_stop_all_if(self) -> ReProgress {
        Re::stop_all_if(self.make_seq())
    }

    pub fn into_opt(self) -> ReProgress {
        Re::opt(self.make_seq())
    }

    pub fn into_var(self, name: &str) -> ReProgress {
        Re::var(name, self.make_seq())
    }

    pub fn into_var_label(self, name: &str) -> ReProgress {
        Re::var_label(name, self.make_seq())
    }

    pub fn into_label(self, name: &str) -> ReProgress {
        Re::label(name, self.make_seq())
    }

    pub fn into_or<T: IntoRe>(self, other: T) -> ReProgress {
        Re::any_of((self.make_seq(), other))
    }

    pub fn into_begin_of(self) -> ReProgress {
        Re::begin_of(self)
    }

    // BUILDER
    pub fn compile(self) -> ReCompiled {
        let re = self.make_seq().into_ire();
        if re.compilation_required() {
            ReCompiled::new(re.compile(None))
        } else {
            ReCompiled::new(re)
        }
    }

    #[inline(always)]
    fn make_seq(self) -> re::seq::Re {
        re::seq::Re::new(self.seq)
    }
}

impl IntoRe for ReProgress {
    fn into_ire(self) -> Rc<dyn IRe> {
        re::seq::Re::new_ire(self.seq)
    }
}

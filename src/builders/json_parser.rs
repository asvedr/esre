use serde_json::{Map, Value};
use std::rc::Rc;

use crate::entities::re::ParseJsonError;
use crate::proto::builders::IJsonParser;
use crate::re;
use crate::{IRe, IntoRe};

type OptParserResult = Result<Option<Rc<dyn IRe>>, ParseJsonError>;
type Builder = Box<dyn Fn(&dyn IJsonParser, &Map<String, Value>) -> OptParserResult>;

pub struct Parser {
    builders: Vec<Builder>,
}

impl Parser {
    pub fn new() -> Parser {
        let builders: Vec<Builder> = vec![
            Box::new(re::any_of::Re::parse_dict),
            Box::new(re::char_range::Re::parse_dict),
            Box::new(re::label::Re::parse_dict),
            Box::new(re::match_str::Re::parse_dict),
            Box::new(re::not::Re::parse_dict),
            Box::new(re::opt::Re::parse_dict),
            Box::new(re::repeat::Re::parse_dict),
            Box::new(re::seq::Re::parse_dict),
            Box::new(re::var::Re::parse_dict),
            Box::new(re::begin_of::Re::parse_dict),
        ];
        Parser { builders }
    }

    fn str_to_re(&self, src: &str) -> Result<Rc<dyn IRe>, ParseJsonError> {
        match src {
            "begin" => Ok(re::begin::Re::new().into_ire()),
            "end" => Ok(re::end::Re::new().into_ire()),
            "any" => Ok(re::any::Re::new().into_ire()),
            _ => ParseJsonError::new(&format!("invalid key-string: {}", src)),
        }
    }

    fn dict_to_re(
        &self,
        dict: &Map<String, Value>,
    ) -> Result<Rc<dyn IRe>, ParseJsonError> {
        for builder in self.builders.iter() {
            if let Some(val) = builder(self, dict)? {
                return Ok(val);
            }
        }
        ParseJsonError::new("json not recognized")
    }
}

impl IJsonParser for Parser {
    fn parse_json(&self, value: &Value) -> Result<Rc<dyn IRe>, ParseJsonError> {
        match value {
            Value::String(ref key) => self.str_to_re(key),
            Value::Object(ref dict) => self.dict_to_re(dict),
            _ => ParseJsonError::new("not only str or dict are available"),
        }
    }
}

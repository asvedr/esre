use std::collections::BTreeMap;
use std::rc::Rc;

use crate::entities::re::{ApplyResponse, Match, State};
use crate::proto::re::IRe;
use serde_json::Value;

#[derive(Clone)]
pub struct ReCompiled {
    re: Rc<dyn IRe>,
}

impl ReCompiled {
    pub(crate) fn new(re: Rc<dyn IRe>) -> ReCompiled {
        ReCompiled { re }
    }

    pub fn match_begin<'a>(&self, text: &'a str) -> Option<Match<'a>> {
        let mut state = State::new(text);
        let matched = match self.re.apply(&mut state, 0) {
            Ok(val) => val,
            _ => return None,
        };
        Some(self.build_match(text, matched))
    }

    pub fn find<'a>(&self, text: &'a str) -> Option<Match<'a>> {
        let mut state = State::new(text);
        let mut index = 0;
        loop {
            if let Ok(val) = self.re.apply(&mut state, index) {
                return Some(self.build_match(text, val));
            };
            index += 1;
            if index >= state.text.len() {
                return None;
            }
        }
    }

    pub fn find_all<'a>(&self, text: &'a str) -> Vec<Match<'a>> {
        let mut state = State::new(text);
        let mut result = Vec::new();
        let mut index = 0;
        loop {
            if let Ok(val) = self.re.apply(&mut state, index) {
                index = val.index;
                result.push(self.build_match(text, val));
            } else if index + 1 >= state.text.len() {
                break;
            } else {
                index += 1;
            }
        }
        result
    }

    pub fn replace(&self, source: &str, value: &str) -> String {
        let matches = self.find_all(source);
        let mut begin = 0;
        let mut result = String::new();
        for match_ in matches {
            let rng = match match_.phrase {
                Some(rng) => rng,
                _ => continue,
            };
            result.push_str(&source[begin..rng.begin]);
            result.push_str(value);
            begin = rng.end + 1;
        }
        result.push_str(&source[begin..]);
        result
    }

    pub fn show(&self) -> String {
        self.re.show()
    }

    pub fn to_json(&self) -> Value {
        self.re.to_json()
    }

    fn build_match<'a>(&self, text: &'a str, resp: ApplyResponse) -> Match<'a> {
        let vars = resp.vars.unwrap_or_else(BTreeMap::new);
        Match::new(resp.range, text, vars)
    }
}

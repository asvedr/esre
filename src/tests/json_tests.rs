use crate::Re;

#[test]
fn check_serialize() {
    let re = Re::begin()
        .any()
        .any_of((Re::ranges('a'..'z'), Re::end()))
        .any_of_greedy(("bam", "bom"))
        .label("x", Re::var("y", Re::any()))
        .opt(Re::create("0").into_one_or_more().join("1"))
        .not("!")
        .one_or_more(Re::ranges("xy"))
        .var_label("x", Re::any())
        .var_label("y", Re::any().into_var("z"))
        .stop_all_if("?")
        .begin_of(Re::ranges('0'..='9'))
        .compile();
    let text = re.show();
    let json = re.to_json();
    let restored_re = Re::parse_json(&json).unwrap().compile();
    let restored_text = restored_re.show();
    assert_eq!(text, restored_text);
}

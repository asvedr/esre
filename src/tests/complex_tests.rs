use rstest::*;

use crate::builders::Re;
use crate::entities::re::Match;

#[rstest(
    source,
    match_found,
    search_found,
    case("hello world", true, true),
    case(" hello world", false, true),
    case("helo world", false, false),
    case("helo hello", false, true),
    case("", false, false)
)]
fn check_match_str(source: &'static str, match_found: bool, search_found: bool) {
    let compiled = Re::create("hello").compile();
    assert_eq!(compiled.match_begin(source).is_some(), match_found);
    assert_eq!(compiled.find(source).is_some(), search_found);
}

fn check_result(got: Option<Match<'_>>, expected: Option<&str>) {
    assert_eq!(got.is_some(), expected.is_some());
    if got.is_none() {
        return;
    }
    assert_eq!(got.unwrap().get_string(), expected.unwrap());
}

#[rstest(
    source,
    result,
    case("", None),
    case("123", None),
    case("+_+_+foo.bar111", Some("f")),
    case(".beba", Some(".")),
    case("a b", Some("a")),
    case("!a b", Some("a"))
)]
fn check_char_range(source: &'static str, result: Option<&'static str>) {
    let compiled = Re::ranges((('a'..='z'), '.')).compile();
    let found = compiled.find(source);
    check_result(found, result);
}

#[rstest(
    source,
    result,
    case("", None),
    case("123", None),
    case("+_+_+foo.bar111", Some("foo.bar")),
    case("beba", Some("beba")),
    case("a b", Some("a")),
    case("!a b", Some("a"))
)]
fn check_repeat_char_range(source: &'static str, result: Option<&'static str>) {
    let compiled = Re::ranges((('a'..='z'), '.')).into_one_or_more().compile();
    let found = compiled.find(source);
    check_result(found, result);
}

#[rstest(
    source,
    result,
    case("", None),
    case("123", None),
    case("hello", Some("hello")),
    case("hello?", Some("hello")),
    case("hello!", Some("hello!")),
    case("hello!!", Some("hello!")),
    case("hello!x", Some("hello!"))
)]
fn check_opt(source: &'static str, result: Option<&'static str>) {
    let compiled = Re::ranges('a'..='z').into_one_or_more().opt("!").compile();
    let found = compiled.find(source);
    check_result(found, result);
}

#[rstest(
    source,
    result,
    var,
    case("", None, None),
    case("123", None, None),
    case("hello", Some("hello"), None),
    case("hello jan", Some("hello jan"), Some("jan")),
    case("hello   james", Some("hello   james"), Some("james")),
    case("hello!", Some("hello"), None)
)]
fn check_opt_var(source: &str, result: Option<&str>, var: Option<&str>) {
    let var = var.map(|x| x.to_string());
    let compiled = Re::create("hello")
        .opt(Re::one_or_more(" ").var("name", Re::ranges('a'..='z').into_one_or_more()))
        .compile();
    let found = compiled.find(source);
    check_result(found.clone(), result);
    if result.is_none() {
        return;
    }
    let var_found = found.unwrap().get_var("name");
    assert_eq!(var_found, var);
}

#[test]
fn check_var_sequence() {
    let source: &str = "hello mike, hello garry, hello nick";
    let first_hello =
        Re::create("hello ").var("name", Re::ranges('a'..='z').into_one_or_more());
    let other_hello = Re::create(", ").join(first_hello.clone());
    let compiled = first_hello.zero_or_more(other_hello).compile();
    let found = compiled.match_begin(source).unwrap();
    assert_eq!(found.get_var("name"), Some("mike".to_string()));
    assert_eq!(found.get_vars("name"), vec!["mike", "garry", "nick"]);
}

#[rstest(
    source,
    case("name is michael, surname is scott"),
    case("surname is scott, name is michael")
)]
fn check_var_layers(source: &'static str) {
    let var_name = Re::ranges('a'..='z').into_one_or_more().into_var("name");
    let n1 = Re::create("name is ")
        .join(var_name.clone())
        .into_var_label("n1");
    let n2 = Re::create("surname is ")
        .join(var_name)
        .into_var_label("n2");
    let order1 = n1.clone().join(", ").join(n2.clone());
    let order2 = n2.join(", ").join(n1);
    let compiled = Re::any_of((order1, order2)).compile();
    let found = compiled.match_begin(source).unwrap();
    assert_eq!(found.get_var("n1.name"), Some("michael".to_string()));
    assert_eq!(found.get_var("n2.name"), Some("scott".to_string()));
}

#[rstest(
    source,
    matches,
    case("name is michael, surname is scott", false),
    case("http://x.com", true),
    case("http://x.com/", true),
    case("http://x/.com/", false),
    case("http://x./com/", false),
    case("http://x.", false),
    case("http://x", false),
    case("http://mail.google.com/", true),
    case("https://mail.google.com", true)
)]
fn check_url(source: &'static str, matches: bool) {
    let word = Re::ranges('a'..='z').into_one_or_more();
    let re = Re::create("http")
        .opt("s")
        .join("://")
        .join(word.clone())
        .one_or_more(Re::create(".").join(word))
        .opt("/")
        .compile();
    assert_eq!(re.match_begin(source).is_some(), matches);
}

#[rstest(
    source,
    expected_full,
    expected_greedy,
    case("", None, None),
    case("li", None, None),
    case("list", Some("list"), Some("list")),
    case("list ", Some("list"), Some("list")),
    case("list or", Some("list or"), Some("list")),
    case("list or ", Some("list or"), Some("list")),
    case("list or tu", Some("list or"), Some("list")),
    case("list or tuple", Some("list or tuple"), Some("list"))
)]
fn check_or(
    source: &'static str,
    expected_full: Option<&'static str>,
    expected_greedy: Option<&'static str>,
) {
    let non_greedy = Re::any_of(("list", "list or tuple", "list or")).compile();
    let greedy = Re::any_of_greedy(("list", "list or tuple", "list or")).compile();
    check_result(non_greedy.match_begin(source), expected_full);
    check_result(greedy.match_begin(source), expected_greedy);
}

#[rstest(source, is_some, case("axb", true), case("ab", false))]
fn check_any(source: &'static str, is_some: bool) {
    let result = Re::create("a")
        .any()
        .join("b")
        .compile()
        .match_begin(source);
    assert_eq!(result.is_some(), is_some);
}

#[rstest(source, var_value, case("ab", Some("b")), case("a", None))]
fn check_any_var(source: &str, var_value: Option<&str>) {
    let var_value = var_value.map(|s| s.to_string());
    let result = Re::create("a")
        .var("x", Re::any())
        .compile()
        .match_begin(source);
    assert_eq!(result.is_some(), var_value.is_some());
    if result.is_none() {
        return;
    }
    assert_eq!(result.unwrap().get_var("x"), var_value);
}

#[rstest(
    source,
    var_value,
    case("ab", None),
    case("hellon:jan?", Some("jan")),
    case("hello , n: he?", Some("he")),
    case("hello , n:he!?", Some("he!"))
)]
fn check_look_forward(source: &str, var_value: Option<&str>) {
    let var_value = var_value.map(|s| s.to_string());
    let re = Re::create("hello")
        .zero_or_more(Re::any())
        .join("n:")
        .zero_or_more(" ")
        .var("name", Re::ranges('a'..='z').into_one_or_more().opt("!"))
        .join("?")
        .compile();
    let result = re.match_begin(source);
    assert_eq!(result.is_some(), var_value.is_some());
    if result.is_none() {
        return;
    }
    assert_eq!(result.unwrap().get_var("name"), var_value);
}

#[rstest(source, matched, case("ababa", false), case("abaxdc", true))]
fn check_look_forward_same_machine(source: &'static str, matched: bool) {
    let re = Re::ranges('a'..='z').into_one_or_more().join("x").compile();
    assert_eq!(re.match_begin(source).is_some(), matched);
}

#[test]
fn check_different_stop_signals() {
    let source = "hello johan";
    let hello = Re::create("hello ");
    let word = Re::ranges('a'..='z').into_one_or_more();
    let joha = Re::create("joha");
    let re1 = hello
        .clone()
        .any_of((Re::create("jo").not("h").join(word.clone()), joha.clone()))
        .compile();
    let re2 = hello
        .any_of((Re::create("jo").stop_all_if("h").join(word), joha))
        .compile();
    assert_eq!(re1.match_begin(source).unwrap().get_string(), "hello joha",);
    assert!(re2.match_begin(source).is_none());
}

#[test]
fn check_optimize_seq_in_seq() {
    let ab = Re::create("a").join("b");
    let cd = Re::create("c").join("d");
    let abcd_re = Re::create((ab, cd)).compile();
    assert_eq!(abcd_re.show(), "seq(\"a\", \"b\", \"c\", \"d\")");
    assert!(abcd_re.match_begin("abc").is_none());
    assert!(abcd_re.match_begin("abcd").is_some());
}

#[test]
fn check_optimize_any_of_in_any_of() {
    let ab = Re::any_of(("a", "b"));
    let cd = Re::any_of(("c", "d"));
    let abcd_re = Re::any_of((ab, cd)).compile();
    assert_eq!(abcd_re.show(), "or(\"a\", \"b\", \"c\", \"d\")");
    assert!(abcd_re.match_begin("r").is_none());
    assert!(abcd_re.match_begin("a").is_some());
    assert!(abcd_re.match_begin("c").is_some());
}

#[test]
fn check_optimize_complex() {
    let ab = Re::any_of(("a", "b"));
    let c = Re::any_of("c");
    let d = Re::create("d");
    let abcd_re = Re::any_of((ab, c, d)).compile();
    assert_eq!(abcd_re.show(), "or(\"a\", \"b\", \"c\", \"d\")");
    assert!(abcd_re.match_begin("r").is_none());
    assert!(abcd_re.match_begin("a").is_some());
    assert!(abcd_re.match_begin("c").is_some());
}

#[rstest(source, matched, case("mark123", true), case(" mark123", false))]
fn check_search_begin(source: &'static str, matched: bool) {
    let mark = Re::create("mark");
    let with_begin = Re::begin().join(mark.clone()).compile();
    let without_begin = mark.compile();
    assert_eq!(with_begin.find(source).is_some(), matched);
    assert!(without_begin.find(source).is_some());
    if !matched {
        return;
    }
    assert_eq!(with_begin.find(source).unwrap().get_string(), "mark",);
}

#[rstest(source, matched, case("mark", true), case("mark123", false))]
fn check_match_end(source: &'static str, matched: bool) {
    let mark = Re::create("mark");
    let with_end = mark.clone().end().compile();
    let without_end = mark.compile();
    assert_eq!(with_end.match_begin(source).is_some(), matched);
    assert!(without_end.match_begin(source).is_some());
    if !matched {
        return;
    }
    assert_eq!(with_end.find(source).unwrap().get_string(), "mark",);
}

#[test]
fn check_begin_end_corner_case() {
    let re = Re::begin().end().compile();
    assert!(re.match_begin("a").is_none());
    let result = re.match_begin("").unwrap();
    assert_eq!(result.get_string(), "");
    let re = Re::begin().end().into_var("key").compile();
    let result = re.match_begin("").unwrap();
    assert_eq!(result.get_string(), "");
    assert_eq!(result.get_var("key"), None);
}

#[test]
fn check_label() {
    let var = Re::ranges('a'..='z').into_one_or_more().into_var("bottom");
    let re = Re::create("hi ").label("top", var).compile();
    let result = re.match_begin("hi derek").unwrap();
    assert_eq!(result.get_string(), "hi derek");
    assert_eq!(result.get_var("top.bottom"), Some("derek".to_string()));
    assert_eq!(result.get_var("top"), None);
}

#[test]
fn check_find_all() {
    let re = Re::ranges('a'..='z').into_one_or_more().compile();
    let source = " hi   mr.  kek";
    let result = re.find_all(source);
    assert_eq!(result.len(), 3);
    assert_eq!(result[0].get_string(), "hi");
    assert_eq!(result[1].get_string(), "mr");
    assert_eq!(result[2].get_string(), "kek");
}

#[rstest(
    source,
    result,
    case(" hi mr. kek did.", " hi ! kek !"),
    case("", ""),
    case("beb", "beb")
)]
fn check_replace(source: &'static str, result: &'static str) {
    let re = Re::ranges('a'..='z').into_one_or_more().join(".").compile();
    assert_eq!(re.replace(source, "!"), result);
}

#[rstest(source, result, case("123abcdn", "abc"), case(" a123", "a12"))]
fn check_begin_of(source: &'static str, result: &'static str) {
    let re = Re::ranges('a'..='z')
        .into_begin_of()
        .any()
        .any()
        .any()
        .compile();
    assert_eq!(re.find(source).unwrap().get_string(), result);
}

use crate::{IntoRe, Re, ReCompiled};
use std::collections::BTreeMap;

const TIMEZONES: &[(&str, i64)] = &[("MSK", 3), ("UTC", 0)];

fn ya_bug_re(date_map: &BTreeMap<String, ()>) -> ReCompiled {
    let nums = Re::ranges('0'..='9');
    let space = Re::one_or_more(' ');
    let time_re = nums
        .clone()
        .into_one_or_more()
        .opt(Re::create(':').one_or_more(nums.clone()))
        .into_var("time");
    let tz_re = Re::any_of_greedy(
        TIMEZONES
            .iter()
            .map(|(key, _)| Re::create(*key).into_ire())
            .collect::<Vec<_>>(),
    )
    .into_var("tz");
    let date_re = Re::any_of_greedy(
        date_map
            .keys()
            .map(|key| Re::create(&**key).into_ire())
            .collect::<Vec<_>>(),
    )
    .into_var("date");
    let variant = Re::any_of((time_re, tz_re, date_re));
    variant
        .clone()
        .opt(
            space
                .clone()
                .join(variant.clone())
                .opt(space.clone().join(variant)),
        )
        .compile()
}

#[test]
fn test_ya_bug() {
    let map = [("завтра".to_string(), ()), ("сегодня".to_string(), ())]
        .into_iter()
        .collect::<BTreeMap<_, _>>();
    let re = ya_bug_re(&map);
    let matched = match re.match_begin("сегодня 23:30") {
        Some(val) => val,
        None => panic!(),
    };
    let time = matched.get_var("time").unwrap();
    assert_eq!(time, "23:30");
}
